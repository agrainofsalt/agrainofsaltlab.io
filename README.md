# Welcome

This simple project relies upon [RStudio](https://www.rstudio.com/), [blogdown](https://bookdown.org/yihui/blogdown/), the [hugo-academic theme](https://github.com/gcushen/hugo-academic), and this [gitlab-ci.yml](https://gitlab.com/rgaiacs/blogdown-gitlab/blob/master/.gitlab-ci.yml).

The automatically generated site is https://agrainofsalt.gitlab.io/

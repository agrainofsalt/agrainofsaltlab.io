+++
# Date this page was created.
date = 2016-04-27T00:00:00

# Project title.
title = "A Grain of Salt"

# Project summary to display on homepage.
summary = "Solving small problems one step at a time"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "bubbles.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["automate-repetition", "human-error", "libre-calc", "libre-writer", "template"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = "A Grain of Salt :smile:"

+++

# Solving small problems one step at a time

This simple project relies upon [RStudio](https://www.rstudio.com/), [blogdown](https://bookdown.org/yihui/blogdown/), the [hugo-academic theme](https://github.com/gcushen/hugo-academic), and [GitLab](https://gitlab.com).

You can view the source code as well as make suggestions or corrections at https://gitlab.com/agrainofsalt/agrainofsalt.gitlab.io/.

-----

## Solutions

### Read the post: [Eliminate mistakes from your cover letters for job applications](../../post/eliminate-mistakes-from-your-cover-letters-for-job-applications/)

- Download the solution: [Job application cover letter Template](../../files/Job application cover letter.ott)

### Read the post: [Reduce time making custom charts for quarterly reports](../../post/reduce-time-making-custom-charts-for-quarterly-reports/)

- Download the solution: [Quarterly reports charts Template](../../files/Quarterly reports charts.ots)

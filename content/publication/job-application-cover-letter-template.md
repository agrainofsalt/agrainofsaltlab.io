+++
title = "Job application cover letter template"
date = 2018-04-17T00:00:00

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["A Grain of Salt"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["3"]

# Publication name and optional abbreviated version.
# publication = "In *International Conference on Multimedia and Expo Workshops (ICMEW)*, IEEE."
# publication_short = "In *ICMEW*"

# Abstract and optional shortened version.
abstract = "Have you ever sent a job application only to realize that you said thank you to the wrong employer? Libre Writer lets you easily create fancy looking cover letters ([and other documents](https://www.techrepublic.com/blog/linux-and-open-source/three-great-ways-to-use-variables-in-libreoffice-and-openoffice/)) with variables such as `<Employer>` or `<Skills>` that will save you time and tears."
abstract_short = "Eliminate cover letter mistakes with template variables"

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
projects = ["a-grain-of-salt"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "../../files/Job application cover letter.ott"

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
url_custom = [{name = "Post", url = "../../post/eliminate-mistakes-from-your-cover-letters-for-job-applications/"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "../post/2018-04-17-eliminate-mistakes-from-your-cover-letters-for-job-applications_files/final.png"
caption = "Job application cover letter template | A Grain of Salt :smile:"

+++

More detail can easily be written here using *Markdown* and $\rm \LaTeX$ math code.
